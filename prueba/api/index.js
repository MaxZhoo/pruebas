const bull =  require("bull") 

const redis = {

    host : "192.168.19.128",
    port : 6379,

}

const opts = {redis: {host: redis.host, port:redis.port }};

const bullCreate  = bull( "curso:create", opts )

const bullDelete  = bull( "curso:delete", opts )

const bullUpdate  = bull( "curso:update", opts )

const bullFindOne = bull( "curso:findone", opts )

const bullView    = bull( "curso:view", opts )

const Create  = async({ name, age, color }) => {

    try {
        
        const job = await bullCreate.add({ name, age, color });

        const { statusCode, data, message } = await job.finished();
    
        /*if (statusCode===200) {
                console.log("bienvenido",data.name);             
         }else{
             console.error(message)
         }*/

         return { statusCode, data, message }

    } catch (error) {

        console.log(error);

    }
}

const Update  = async({name,age,color,id}) => {

    try {
        
        const job = await bullUpdate.add({name,age,color,id});

        const { statusCode, data, message } = await job.finished();
        /*if (statusCode===200) {
        
            console.log("El usuario fue actualizado ",data);

        } else {

            console.error(message)

        } */

        return { statusCode, data, message };

    } catch (error) {

        console.log(error);

    }
}
const FindOne  = async({id}) => {

    try {
        
        const job = await bullFindOne.add({id});

        const { statusCode, data, message } =await job.finished();
    
        return { statusCode, data, message };

    } catch (error) {

        console.log(error);

    }
}
const View  = async({ }) => {

    try {
        
        const job = await bullView.add({ });

        const { statusCode, data, message } = await job.finished();
    
       /* if (statusCode===200) {

            data.forEach(x => {
                console.log(x);
            });

        }else{

            console.error(message)
        
        }*/

        return { statusCode, data, message }

    } catch (error) {

        console.log(error);

    }
}
const Delete  = async({id}) => {

    try {
        
        const job = await bullDelete.add({id});

        const { statusCode, data, message } = await job.finished();

        /*if (statusCode===200) {
        
            console.log("El usuario eliminado fue",data.name);

        } else {

            console.error(message)

        }*/

        return { statusCode, data, message } 

    } catch (error) {

        console.log(error);

    }
}
const main = async()=>{

   //await Delete({id:21})
   //await Create({name:"Monica",color:"tomato",age:20})
   //await FindOne({id:31})
   
   // await Update({age:31,id:31})


   //await View({});
}

//main()

module.exports = { Create, Delete, FindOne, Update, View }