const express = require("express");
const app = express();
const port =  80;
const api = require("../api/index")
const http = require("http");

const server  =  http.createServer(app);

const {Server} = require("socket.io");

const io = new Server(server);

server.listen(port,()=>{

    console.log(`server is running on port ${port}`);

    io.on("connection",(socket)=>{

        console.log(`New connection ${socket.id}`);

        socket.on("req:microservice:view", async({ })=>{

            try {
                
                console.log("req:microservice:view");

                const { statusCode, data, message } = await api.View({})
    
                return io.to(socket.id).emit("res:microservice:view", { statusCode, data, message })

            } catch (error) {

                console.log(error);
            
            }

        })

        socket.on("req:microservice:create",async({ name, age, color })=>{

            try {
                
                console.log("req:microservice:create",{ name, age, color });

                const {statusCode, data, message} = await api.Create({name,age,color})

                return io.to(socket.id).emit("res:microservice:create", { statusCode, data, message })

            } catch (error) {
                console.log(error);   
            }
        })

        socket.on("req:microservice:delete", async({ id })=>{

            try {

                console.log("req:microservice:delete",({ id }));

                const {statusCode, data, message } = await api.Delete({id})
    
                return io.to(socket.id).emit("res:microservice:delete", { statusCode, data, message })
                
            } catch (error) {
                
                console.log(error);

            }
        })

        socket.on("req:microservice:update",async({name,age,color,id})=>{

            try {
                
                console.log("req:microservice:update",({name,age,color,id}));

                const {statusCode, data, message} = await api.Update({name,age,color,id});
    
                return io.to(socket.id).emit("res:microservice:update", { statusCode, data, message })

            } catch (error) {
                
                console.log(error);
            
            }
        })

        socket.on("req:microservice:findOne",async({id})=>{

            try {

                console.log("req:microservice:findOne",({id}));

                const {statusCode, data, message} = await api.FindOne({id})
    
                return io.to(socket.id).emit("res:microservice:update", { statusCode, data, message })
                
            } catch (error) {
  
                console.log(error);
  
            }
        })

    
    })
})