const Services = require("../Services")
const {internalError} = require("../settings")
const {bullView,bullCreate,bullDelete,bullUpdate,bullFindOne} = require("./index")


const View = async (job,done)=>{

    try {
        
        const { } = job.data

        console.log(job.id);

        let {statusCode,data,message} = await Services.View({ });

        if (statusCode!==200) throw(message)

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullView",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
const Create = async (job,done)=>{
 
    try {
        
        const {name,age,color} = job.data

        let {statusCode,data,message} = await Services.Create({name,age,color});

        if (statusCode!==200) throw(message)

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullCreate",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
const Delete = async (job,done)=>{
     
    try {
        
        const {id} = job.data

        let {statusCode,data,message} = await Services.Delete({id});

        if (statusCode!==200) throw(message)

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullDelete",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
const Update = async (job,done)=>{

    try {
        
        const {name,age,color,id} = job.data

        let {statusCode,data,message} = await Services.Update({name,age,color,id});

        if (statusCode!==200) throw(message)

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullUpdate",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
const FindOne = async (job,done)=>{
     
    try {
        
        const {id} = job.data

        let {statusCode,data,message} = await Services.FindOne({id});

        if (statusCode!==200) throw(message)

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullFindOne",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}

async function run(){

    try {

        console.log("vamos inicializar worker");

        bullView.process(View);
        bullCreate.process(Create);
        bullDelete.process(Delete);
        bullUpdate.process(Update);
        bullFindOne.process(FindOne);
        
    } catch (error) {

        console.log(error);

    }

}
module.exports = { View, Delete, Create, FindOne, Update, run }