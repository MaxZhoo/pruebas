const { DataRowMessage } = require("pg-protocol/dist/messages");
const {Model,syncDB} = require("../Models");

//const {setTimeout}  =require("timers/promises")
//await setTimeout(3000);

const Create = async ({name,age,color}) =>{

    try {

        let user1 = await Model.create({ //method create make build and save methods

            name,age,color},{fields:["name","age","color"],logging:false}
    
            );
    
        return { statusCode: 200 , data: user1.toJSON() }
        
    } catch (error) {

        console.log({step:"Controllors Create",error: error.toString});

        return { statusCode: 500, message: error.toString() }

    }

}

const Delete = async ({id}) =>{

    try {

        const findUser = await Model.findByPk(id,{logging:false});

        if (findUser === null) {

            throw('User Not found!');

          } else {

            //const dato = findUser.toJSON()

            await findUser.destroy()
  
            return {statusCode:200,message:"ok se elimino el usuario"/*,data: dato */}

          }
        
    } catch (error) {

        console.log({step:"Controllers Delete",error: error.toString()});

        return { statusCode: 500, message: error.toString() }
        
    }

}

const Update = async ({name,age,color,id}) =>{

    try {

        let user1 = await Model.update( {name,age,color} , {where: {id} , logging:false , returning: true});
    
        return { statusCode: 200 , data: user1[1][0].toJSON() }

    } catch (error) {

        console.log({step:"Controllors Update", error: error.toString});

        return { statusCode: 500, message: error.toString() }
        
    }

}

const FindOne = async ({where={}}) =>{

    try {

        const findUser = await Model.findOne({where,logging:false});

        if(findUser) return { statusCode: 200 , data: findUser.toJSON() }
        else return {statusCode: 400, message: "no existe el usuario"}

    } catch (error) {

        console.log({step:"Controllors FindOne",error: error.toString});

        return { statusCode: 500, message: error.toString() }
        
    }

}

const View = async ({ where={ } }) =>{

    try {

        const instances = await Model.findAll({ where, logging:false});

        return { statusCode: 200 , data: instances }
        
    } catch (error) {

        console.log({step:"Controllors view",error: error.toString});

        return { statusCode: 500, message: error.toString() }
        
    }

}

module.exports = {Delete,View,Create,FindOne,Update}