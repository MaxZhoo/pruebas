const {sequelize} = require("../settings");
const DataTypes = require("sequelize");
const { Field } = require("pg-protocol/dist/messages");


const Model = sequelize.define("curso",{

    name: {type: DataTypes.STRING},
    age: {type:DataTypes.BIGINT},
    color: {type: DataTypes.STRING},

});

const syncDB = async () =>{
    
    try {

        console.log("vamos a inicializar la base de datos");

        await Model.sync({logging:false})

        console.log("base de datos inicializada");

        return {statusCode: 200, data: "ok"}

    } catch (error) {
        
        console.log(error);

        return {statuscode: 500, message: error.toString()}

    }

}


module.exports = {Model, syncDB}