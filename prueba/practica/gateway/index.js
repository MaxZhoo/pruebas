const express = require("express");
const app = express();
const port =  80;

//apis
const apiLibros = require("api-libro");
const apiPagos = require("api-pagos")
const apiSocio = require("api-socio")


const http = require("http");

const server  =  http.createServer(app);

const {Server} = require("socket.io");

const io = new Server(server);

server.listen(port,()=>{

    console.log(`server is running on port ${port}`);

    io.on("connection",(socket)=>{

        console.log(`New connection ${socket.id}`);

        // Libros

        socket.on("req:libros:view", async({ })=>{

            try {
                
                console.log("req:libros:view");

                const { statusCode, data, message } = await apiLibros.View({})
    
                return io.to(socket.id).emit("res:libros:view", { statusCode, data, message })

            } catch (error) {

                console.log(error);
            
            }

        })

        socket.on("req:libros:create",async({ title })=>{

            try {
                
                console.log("req:libros:create",{ name, age, color });

                const {statusCode, data, message} = await apiLibros.Create({title})

                return io.to(socket.id).emit("res:libros:create", { statusCode, data, message })

            } catch (error) {
                console.log(error);   
            }
        })

        socket.on("req:libros:delete", async({ id })=>{

            try {

                console.log("req:libros:delete",({ id }));

                const {statusCode, data, message } = await apiLibros.Delete({id})
    
                return io.to(socket.id).emit("res:libros:delete", { statusCode, data, message })
                
            } catch (error) {
                
                console.log(error);

            }
        })

        socket.on("req:libros:update",async({title,seccions,category,id})=>{

            try {
                
                console.log("req:libros:update",({name,age,color,id}));

                const {statusCode, data, message} = await apiLibros.Update({title,seccions,category,id});
    
                return io.to(socket.id).emit("res:libros:update", { statusCode, data, message })

            } catch (error) {
                
                console.log(error);
            
            }
        })

        socket.on("req:libros:findOne",async({title})=>{

            try {

                console.log("req:libros:findOne",({id}));

                const {statusCode, data, message} = await apiLibros.FindOne({title})
    
                return io.to(socket.id).emit("res:libros:findOne", { statusCode, data, message })
                
            } catch (error) {
  
                console.log(error);
  
            }
        })

        // Socios

        socket.on("req:socios:view", async({ enable })=>{

            try {
                
                console.log("req:socios:view");

                const { statusCode, data, message } = await apiSocio.View({enable})
    
                return io.to(socket.id).emit("res:socios:view", { statusCode, data, message })

            } catch (error) {

                console.log(error);
            
            }

        })

        socket.on("req:socios:create",async({ name, phone })=>{

            try {
                
                console.log("req:socios:create",{ name, age, color });

                const {statusCode, data, message} = await apiSocio.Create({ name, phone})

                return io.to(socket.id).emit("res:socios:create", { statusCode, data, message })

            } catch (error) {
                console.log(error);   
            }
        })

        socket.on("req:socios:delete", async({ id })=>{

            try {

                console.log("req:socios:delete",({ id }));

                const {statusCode, data, message } = await apiSocio.Delete({ id})
    
                return io.to(socket.id).emit("res:socios:delete", { statusCode, data, message })
                
            } catch (error) {
                
                console.log(error);

            }
        })

        socket.on("req:socios:update",async({age,id,name,email,phone})=>{

            try {
                
                console.log("req:socios:update",({name,age,color,id}));

                const {statusCode, data, message} = await apiSocio.Update({ age,id,name,email,phone});
    
                return io.to(socket.id).emit("res:socios:update", { statusCode, data, message })

            } catch (error) {
                
                console.log(error);
            
            }
        })

        socket.on("req:socios:findOne",async({id})=>{

            try {

                console.log("req:socios:findOne",({id}));

                const {statusCode, data, message} = await apiSocio.FindOne({id})
    
                return io.to(socket.id).emit("res:socios:findOne", { statusCode, data, message })
                
            } catch (error) {
  
                console.log(error);
  
            }
        })

        socket.on("req:socios:disable",async({id})=>{

            try {

                console.log("req:socios:disable",({id}));

                const {statusCode, data, message} = await apiSocio.Disable({id})
    
                return io.to(socket.id).emit("res:socios:disable", { statusCode, data, message })
                
            } catch (error) {
  
                console.log(error);
  
            }
        })

        socket.on("req:socios:enable",async({id})=>{

            try {

                console.log("req:socios:enable",({id}));

                const {statusCode, data, message} = await apiSocio.Enable({id})
    
                return io.to(socket.id).emit("res:socios:enable", { statusCode, data, message })
                
            } catch (error) {
  
                console.log(error);
  
            }
        })
        // Pagos

        socket.on("req:pagos:view", async({ })=>{

            try {
                
                console.log("req:pagos:view");

                const { statusCode, data, message } = await apiPagos.View({})

                return io.to(socket.id).emit("res:pagos:view", { statusCode, data, message })

            } catch (error) {

                console.log(error);
            
            }

        })

        socket.on("req:pagos:create",async({ socio, amount})=>{

            try {
                
                console.log("req:pagos:create",{ socio, amount});

                const {statusCode, data, message} = await apiPagos.Create({socio, amount})

                return io.to(socket.id).emit("res:pagos:create", { statusCode, data, message })

            } catch (error) {
                console.log(error);   
            }
        })

        socket.on("req:pagos:delete", async({ id })=>{

            try {

                console.log("req:pagos:delete",({ id }));

                const {statusCode, data, message } = await apiPagos.Delete({id})

                return io.to(socket.id).emit("res:pagos:delete", { statusCode, data, message })
                
            } catch (error) {
                
                console.log(error);

            }
        })

        socket.on("req:pagos:findOne",async({id})=>{

            try {

                console.log("req:pagos:findOne",({id}));

                const {statusCode, data, message} = await apiPagos.FindOne({id})

                return io.to(socket.id).emit("res:pagos:findOne", { statusCode, data, message })
                
            } catch (error) {

                console.log(error);

            }
        })

    })
})