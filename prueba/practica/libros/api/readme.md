## API de Libros

Esta es una api para interactuar con los libros registrados en la biblioteca

``` js

const apiLibros = require("api-libro");

socket.on("req:libros:view", async({ })=>{

    try {
        
        console.log("req:libros:view");

        const { statusCode, data, message } = await apiLibros.View({})

        return io.to(socket.id).emit("res:libros:view", { statusCode, data, message })

    } catch (error) {

        console.log(error);
    
    }

})

socket.on("req:libros:create",async({ title })=>{

    try {
        
        console.log("req:libros:create",{ name, age, color });

        const {statusCode, data, message} = await apiLibros.Create({title})

        return io.to(socket.id).emit("res:libros:create", { statusCode, data, message })

    } catch (error) {
        console.log(error);   
    }
})

socket.on("req:libros:delete", async({ id })=>{

    try {

        console.log("req:libros:delete",({ id }));

        const {statusCode, data, message } = await apiLibros.Delete({id})

        return io.to(socket.id).emit("res:libros:delete", { statusCode, data, message })
        
    } catch (error) {
        
        console.log(error);

    }
})

socket.on("req:libros:update",async({title,seccions,category,id})=>{

    try {
        
        console.log("req:libros:update",({name,age,color,id}));

        const {statusCode, data, message} = await apiLibros.Update({title,seccions,category,id});

        return io.to(socket.id).emit("res:libros:update", { statusCode, data, message })

    } catch (error) {
        
        console.log(error);
    
    }
})

socket.on("req:libros:findOne",async({title})=>{

    try {

        console.log("req:libros:findOne",({id}));

        const {statusCode, data, message} = await apiLibros.FindOne({title})

        return io.to(socket.id).emit("res:libros:findOne", { statusCode, data, message })
        
    } catch (error) {

        console.log(error);

    }
})

```
Solo se pueden registrar libros de categoría A