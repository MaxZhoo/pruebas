const Controllers = require("../Controllers");
const { internalError } = require("../settings");


const Create = async ({ title }) => {

    try {

        let {statusCode,data,message} = await Controllers.Create({ title })
        
        return { statusCode,data,message }

    } catch (error) {
        
        console.log({step:"Services Create",error:error.toString()});

        return {statusCode:500,message: error.toString()}
    }
        
}  
       
const Delete = async ({ id }) => {

    try {

        const findOne = await Controllers.FindOne({where: {id}})

        if (findOne.statusCode!==200) {

            switch (findOne.statusCode) {
                case 400:
                    return {statusCode: 400 , message: "No existe el usuario a eliminar"}
                    
                case 500:
                    return {statusCode: 500, message: internalError}
                    
                default:
                    return {statusCode: findOne.statusCode, message: findOne.message}
                   
            }   
        }

       const del = await Controllers.Delete({id})

       if(del.statusCode ===200){ 
     
           return {statusCode:200, data: findOne.data}

       }else{

            return { statusCode:400, message: internalError }

       }
       /*
       const {statusCode,data,message} = await Controllers.Delete({id})

       return {statusCode,data,message}*/ //esto funciona con el const que esta declarado en controllers y comentado

    } catch (error) {
        
        console.log({step:"Services Delete",error:error.toString()});

        return {statusCode:500,message: error.toString()}

    }
        
}   

const Update = async ({title, category, seccions, id}) => {

    try {

        let { statusCode, data, message } = await Controllers.Update({ title, category, seccions, id })
        
        return { statusCode, data, message }

    } catch (error) {
        
        console.log({step:"Services Update",error:error.toString()});

        return { statusCode:500, message: error.toString() }
    }
        
}  

const FindOne = async ({ title }) => {

    try {

        let {statusCode,data,message} = await Controllers.FindOne({ where: { title } })
        
        return { statusCode,data,message }

    } catch (error) {
        
        console.log({ step:"Services FindOne", error:error.toString() });

        return {statusCode:500,message: error.toString()}
    }
        
}  
const View = async ({ }) => {

    try {

        let { statusCode, data, message } = await Controllers.View({ })
        
        return { statusCode, data, message }

    } catch (error) {
        
        console.log({ step:"Services View", error:error.toString() });

        return { statusCode:500, message: error.toString() }
    }
        
}  
module.exports = { Create, Delete, FindOne, View, Update }