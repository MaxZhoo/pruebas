const {syncDB} = require("./Models")
const {run} = require("./Adapters/processor")

module.exports = {syncDB,run}