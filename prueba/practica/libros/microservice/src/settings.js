const dotenv = require("dotenv")
const {Sequelize} = require("sequelize")


dotenv.config()

const redis = {

    host : process.env.REDIS_HOST,
    port : process.env.REDIS_PORT,

}

const db={

    host : process.env.POSTGRES_HOST,
    port : process.env.POSTGRES_PORT,
    database : process.env.POSTGRES_DB,
    password : process.env.POSTGRES_PASSWORD,
    username : process.env.POSTGRES_USER,

}  

const internalError = "no se pudo procesar la solicitud";

const sequelize  = new Sequelize({

    host:db.host,
    port:db.port,
    database:db.database,
    password:db.password,
    username:db.username,
    dialect:"postgres",

})


module.exports = {redis,internalError,sequelize}