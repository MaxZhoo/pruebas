const bull =  require("bull") 
const {name} = require("../microservice/package.json")

const redis = {

    host : "192.168.19.128",

    port : 6379,

}

const opts = {redis: {host: redis.host, port:redis.port }};

const bullCreate  = bull( `${name}:create`, opts )

const bullDelete  = bull( `${name}:delete`, opts )

const bullFindOne = bull( `${name}:findone`, opts )

const bullView    = bull( `${name}::view`, opts )

const Create  = async({ socio, amount }) => {

    try {
        
        const job = await bullCreate.add({ socio, amount });

        const { statusCode, data, message } = await job.finished();
    
        /*if (statusCode===200) {
                console.log("bienvenido",data.name);             
         }else{
             console.error(message)
         }*/

         return { statusCode, data, message }

    } catch (error) {

        console.log(error);

    }
}

const FindOne  = async({id}) => {

    try {
        
        const job = await bullFindOne.add({id});

        const { statusCode, data, message } =await job.finished();
    
        return { statusCode, data, message };

    } catch (error) {

        console.log(error);

    }
}
const View  = async({ }) => {

    try {
        
        const job = await bullView.add({ });

        const { statusCode, data, message } = await job.finished();
    
       /* if (statusCode===200) {

            data.forEach(x => {
                console.log(x);
            });

        }else{

            console.error(message)
        
        }*/

        return { statusCode, data, message }

    } catch (error) {

        console.log(error);

    }
}
const Delete  = async({id}) => {

    try {
        
        const job = await bullDelete.add({id});

        const { statusCode, data, message } = await job.finished();

        /*if (statusCode===200) {
        
            console.log("El usuario eliminado fue",data.name);

        } else {

            console.error(message)

        }*/

        return { statusCode, data, message } 

    } catch (error) {

        console.log(error);

    }
}
const main = async()=>{

   //await Delete({id:21})
   //await Create({name:"Monica",color:"tomato",age:20})
   //await FindOne({id:31})
   
   // await Update({age:31,id:31})


   //await View({});
}

//main()

module.exports = { Create, Delete, FindOne, View }