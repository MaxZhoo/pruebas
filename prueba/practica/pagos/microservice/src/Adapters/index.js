const bull =  require("bull")
const {redis} = require("../settings")
 

//console.log("hola mundo, abajo está bull");

const opts = {redis: {host: redis.host, port:redis.port }};


const bullCreate  = bull( "curso:create", opts )

const bullDelete  = bull( "curso:delete", opts )

const bullFindOne = bull( "curso:findone", opts )

const bullView    = bull( "curso:view", opts )

module.exports = { bullView, bullDelete, bullCreate, bullFindOne }