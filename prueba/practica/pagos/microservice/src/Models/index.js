const {sequelize} = require("../settings");
const DataTypes = require("sequelize");
const { Field } = require("pg-protocol/dist/messages");


const Model = sequelize.define("curso",{

    socio: {type: DataTypes.BIGINT},

    amount: {type:DataTypes.BIGINT},

});

const syncDB = async () =>{
    
    try {

        await Model.sync({logging:false})

        return {statusCode: 200, data: "ok"}

    } catch (error) {
        
        console.log(error);

        return {statuscode: 500, message: error.toString()}

    }

}


module.exports = {Model, syncDB}