const bull =  require("bull")

const {name} = require("../microservice/src/settings")

const redis = {

    host : "192.168.19.128",

    port : 6379,

}

const opts = {redis: {host: redis.host, port:redis.port }};

const bullCreate  = bull( `${name}:create`, opts )

const bullDelete  = bull( `${name}:delete`, opts )

const bullUpdate  = bull( `${name}:update`, opts )

const bullFindOne = bull( `${name}:findone`, opts )

const bullView    = bull( `${name}:view`, opts )

const bullEnable    = bull( `${name}:enable`, opts )

const bullDisable  = bull( `${name}:disable`, opts )

const Enable  = async({ id }) => {

    try {
        
        const job = await bullEnable.add({ id});

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {

        console.log(error);

    }
}

const Disable  = async({ id }) => {

    try {
        
        const job = await bullDisable.add({ id });

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {

        console.log(error);

    }
}

const Create  = async({ name, phone }) => {

    try {
        
        const job = await bullCreate.add({ name, phone });

        const { statusCode, data, message } = await job.finished();
    
        /*if (statusCode===200) {
                console.log("bienvenido",data.name);             
         }else{
             console.error(message)
         }*/

        return { statusCode, data, message }

    } catch (error) {

        console.log(error);

    }
}

const Update  = async({name,age,phone,id,email}) => {

    try {
        
        const job = await bullUpdate.add({name,age,phone,id,email});

        const { statusCode, data, message } = await job.finished();
        /*if (statusCode===200) {
        
            console.log("El usuario fue actualizado ",data);

        } else {

            console.error(message)

        } */

        return { statusCode, data, message };

    } catch (error) {

        console.log(error);

    }
}
const FindOne  = async({id}) => {

    try {
        
        const job = await bullFindOne.add({id});

        const { statusCode, data, message } =await job.finished();
    
        return { statusCode, data, message };

    } catch (error) {

        console.log(error);

    }
}
const View  = async({ enable }) => {

    try {
        
        const job = await bullView.add({ enable });

        const { statusCode, data, message } = await job.finished();
    
        /*if (statusCode===200) {

            data.forEach(x => {
                console.log(x);
            });

        }else{

            console.error(message)
        
        }*/

        return { statusCode, data, message }

    } catch (error) {

        console.log(error);

    }
}
const Delete  = async({id}) => {

    try {
        
        const job = await bullDelete.add({id});

        const { statusCode, data, message } = await job.finished();

        /*if (statusCode===200) {
        
            console.log("El usuario eliminado fue",data.name);

        } else {

            console.error(message)

        }*/

        return { statusCode, data, message } 

    } catch (error) {

        console.log(error);

    }
}
const main = async()=>{

   //await Delete({id:21})
   //await Create({name:"Pepe",age:30,phone:"+51 953 402 647"})
   //await FindOne({id:31})
   
   // await Update({age:31,id:31})


  // await View({enable:true});
}

//main()

module.exports = { Create, Delete, FindOne, Update, View, Enable, Disable }