const bull =  require("bull")
const {redis,name} = require("../settings")
 

//console.log("hola mundo, abajo está bull");

const opts = {redis: {host: redis.host, port:redis.port }};


const bullCreate  = bull( `${name}:create`, opts )

const bullDelete  = bull( `${name}:delete`, opts )

const bullUpdate  = bull( `${name}:update`, opts )

const bullFindOne = bull( `${name}:findone`, opts )

const bullView    = bull( `${name}:view`, opts )

const bullEnable    = bull( `${name}:enable`, opts )

const bullDisable  = bull( `${name}:disable`, opts )

module.exports = { bullView, bullDelete, bullCreate, bullFindOne, bullUpdate, bullEnable, bullDisable }