const Services = require("../Services")
const {internalError} = require("../settings")
const {bullView,bullCreate,bullDelete,bullUpdate,bullFindOne, bullDisable,bullEnable} = require("./index")


const View = async (job,done)=>{

    try {
        
        const { enable } = job.data

        console.log(job.id);

        let {statusCode,data,message} = await Services.View({ enable });

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullView",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
const Create = async (job,done)=>{
 
    try {
        
        const {name,phone} = job.data

        let {statusCode,data,message} = await Services.Create({name,phone});
 
        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullCreate",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
const Delete = async (job,done)=>{
     
    try {
        
        const {id} = job.data

        let {statusCode,data,message} = await Services.Delete({id});

        if (statusCode!==200) throw(message)

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullDelete",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
const Update = async (job,done)=>{

    try {
        
        const {name,age,phone,id,email} = job.data

        let {statusCode,data,message} = await Services.Update({name,age,phone,id,email});
  
        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullUpdate",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
const FindOne = async (job,done)=>{
     
    try {
        
        const {id} = job.data

        let {statusCode,data,message} = await Services.FindOne({id});

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullFindOne",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}

const Enable = async (job,done)=>{
     
    try {
        
        const {id} = job.data

        let {statusCode,data,message} = await Services.Enable({id});

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullEnable",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
const Disable = async (job,done)=>{
     
    try {
        
        const {id} = job.data

        let {statusCode,data,message} = await Services.Disable({id});

        done(null,{statusCode,data,message});

    } catch (error) {

        console.log({step:"Adapters processor bullDisable",error:error.toString()});
        
        done(null,{message: internalError})
    }
    
}
async function run(){

    try {

        console.log("vamos inicializar worker");

        bullView.process(View);
        bullCreate.process(Create);
        bullDelete.process(Delete);
        bullUpdate.process(Update);
        bullFindOne.process(FindOne);
        bullDisable.process(Disable);
        bullEnable.process(Enable);

        
    } catch (error) {

        console.log(error);

    }

}
module.exports = { View, Delete, Create, FindOne, Update, run, Disable, Enable }