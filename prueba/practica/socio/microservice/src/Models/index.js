const {sequelize} = require("../settings");
const DataTypes = require("sequelize");
const { Field } = require("pg-protocol/dist/messages");


const Model = sequelize.define("socio",{

    name: {type: DataTypes.STRING},

    age: {type:DataTypes.BIGINT},
    
    email: {type: DataTypes.STRING},
    
    phone : {type: DataTypes.STRING},

    enable : {type: DataTypes.BOOLEAN},

}, { freezeTableName: true });

const syncDB = async () =>{
    
    try {

        await Model.sync({logging:false})

        return {statusCode: 200, data: "ok"}

    } catch (error) {
        
        console.log(error);

        return {statuscode: 500, message: error.toString()}

    }

}


module.exports = {Model, syncDB}