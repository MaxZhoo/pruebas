const Controllers = require("../Controllers");
const { internalError } = require("../settings");


const Create = async ({ name, phone }) => {

    try {

        let {statusCode, data, message} = await Controllers.Create({name, phone})
        
        return { statusCode, data, message }

    } catch (error) {
        
        console.log({step:"Services Create", error:error.toString()});

        return {statusCode:500, message: error.toString()}
    }
        
}  
       
const Delete = async ({ id }) => {

    try {

        const findOne = await Controllers.FindOne({where: {id}})

        if (findOne.statusCode!==200) {

            switch (findOne.statusCode) {
                case 400:
                    return {statusCode: 400 , message: "No existe el usuario a eliminar"}
                    
                case 500:
                    return {statusCode: 500, message: internalError}
                    
                default:
                    return {statusCode: findOne.statusCode, message: findOne.message}
                   
            }   
        }

       const del = await Controllers.Delete({id})

       if(del.statusCode ===200){ 
     
           return {statusCode:200, data: findOne.data}

       }else{

            return { statusCode:400, message: internalError }

       }
       /*
       const {statusCode,data,message} = await Controllers.Delete({id})

       return {statusCode,data,message}*/ //esto funciona con el const que esta declarado en controllers y comentado

    } catch (error) {
        
        console.log({step:"Services Delete",error:error.toString()});

        return {statusCode:500,message: error.toString()}

    }
        
}   

const Update = async ({name,age,phone,id,email}) => {

    try {

        let { statusCode, data, message } = await Controllers.Update({name,age,phone,id,email })
        
        return { statusCode, data, message }

    } catch (error) {
        
        console.log({step:"Services Update",error:error.toString()});

        return { statusCode:500, message: error.toString() }
    }
        
}  

const FindOne = async ({ id }) => {

    try {

        let {statusCode,data,message} = await Controllers.FindOne({ where: { id } })
        
        return { statusCode,data,message }

    } catch (error) {
        
        console.log({ step:"Services FindOne", error:error.toString() });

        return {statusCode:500,message: error.toString()}
    }
        
}  
const Enable = async ({ id }) => {

    try {

        let {statusCode,data,message} = await Controllers.Enable({id})
        
        return { statusCode,data,message }

    } catch (error) {
        
        console.log({ step:"Services Enable", error:error.toString() });

        return {statusCode:500,message: error.toString()}
    }
        
}  
const Disable = async ({ id }) => {

    try {

        let {statusCode,data,message} = await Controllers.Disable({id})
        
        return { statusCode,data,message }

    } catch (error) {
        
        console.log({ step:"Services Disable", error:error.toString() });

        return {statusCode:500,message: error.toString()}
    }
        
}  
const View = async ({ enable }) => {

    try {

        let { statusCode, data, message } = await Controllers.View({where: {enable} })
        
        return { statusCode, data, message }

    } catch (error) {
        
        console.log({ step:"Services View", error:error.toString() });

        return { statusCode:500, message: error.toString() }
    }
        
}

module.exports = { Create, Delete, FindOne, View, Update, Disable, Enable }