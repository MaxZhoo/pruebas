import React,{ useEffect, useState } from 'react';

import { socket } from "./ws";

function App() {

  const [id,setId] = useState()

  const [data,setData] = useState([])

  
  useEffect(()=>{
    

          setTimeout(()=>{
            
            setId(socket.id)

          },200);

          socket.on("res:microservice:view", ({statusCode, data, message})=>{

            console.log("res:microservice:view", {statusCode,data,message});

            console.log({statusCode,data,message});

            if (statusCode===200) {
              setData(data)
            }

        })

        socket.on("res:microservice:create", ({statusCode, data, message})=>{

            console.log("res:microservice:create",{statusCode,data,message});

        })

        socket.on("res:microservice:findOne", ({statusCode, data, message})=>{

            console.log("res:microservice:findOne",{statusCode,data,message});

        })

        socket.on("res:microservice:delete", ({statusCode, data, message})=>{

            console.log("res:microservice:delete",{statusCode,data,message});

        })

        socket.on("res:microservice:update", ({statusCode, data, message})=>{

            console.log("res:microservice:update",{statusCode,data,message});

        })

          //  socket.emit("req:microservice:create",({age:"24",name:"Joel",color:"blue" }))
          // socket.emit("req:microservice:view",({ }));
         // socket.emit("req:microservice:update",({  id: 35, age:19}));
          //  socket.emit("req:microservice:delete",({id:34 }));

      

  },[])

  return (
    <div >
  
    	{data.map((v,i) => <p key={i}>Nombre: {v.name} Edad: {v.age}</p>)}
    
    </div>
  );
}

export default App;
