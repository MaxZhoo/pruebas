import React, {useEffect, useState} from "react";
import { socket } from "./ws";
import styled from "styled-components"

const Container = styled.div`



`

const App = () =>{

    const [data,setData] = useState([])
  
    socket.on("res:microservice:view", ({statusCode, data, message})=>{

        console.log("res:microservice:view", {statusCode,data,message});

        console.log({statusCode,data,message});

        if (statusCode===200) setData(data)

    })

    setTimeout(() => socket.emit("req:microservice:view", ({})), 1000);

    return (
        <>
            {data.map((v,i) => <p key={i}>Nombre: {v.name} Edad: {v.age}</p>)}
        </>
    )

}


export default App;